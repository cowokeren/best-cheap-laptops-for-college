# Choosing The Best Cheap Laptops for College Students #
We have great and cool mini computers like the Netbooks. Apart from the usual feature of portability, they are also good performers and light in weight. These 12 inch laptops can be categorized in to two types namely:

The Affordable Laptops
The Premium Laptops
The affordable laptops are best for student laptops as they come with decent and handy features but with less sophistication in terms of hardware, they also usually fall into the class of $200 laptops because of the cheaper hardware they use.
The premium ones on the other hand come with everything required with the high price that is expected.
Let us first look at the affordable 12 inch laptops.

The Asus 1201N with Nvidia ION

The Asus 1201N is a 12 inch laptop that has great looks and an exterior that is glossy polished with a slim body. These are one of the best college laptops and they come in colors like black or silver.

This 12 inch laptop has a processor that is a Intel Atom N450, 250 Gb storage space, 2 GB of RAM and the Nvidia ION graphics.
This laptop is priced at $484 with free shipping.

MSI Wind12 U230 with AMD Inside

These 12 inch laptops are good performers in terms of speed and attractive prices at $479 and free shipping. They have a good battery life up to 3-4 hours though it is less compared to the other portable laptops. This is a configuration of 2 GB RAM, 1.6GHz AMD Athlon X2 L335 Processor and 320 GB Storage drive complete with the graphic solution ATI HD 3200.

The Premium 12 inch laptops
Compared to the affordable ones, these 12 inch laptops are expensive.

The HP Touchsmart TM2

The laptops come with wonderful autonomy and hardware. Priced at $1200, this Dual Core SU7300 Processor has 4GB RAM and 320GB hard drive. There is also Intel’s graphics GMA 4500.


 
[Read More](http://bestcheaplaptopnotebook.com/college-laptops/)